<body>
    <div class="wrapper">
        <div class="sidebar" data-active-color="blue" data-background-color="black" data-image="<?= base_url() ?>assets/img/sidebar-1.jpg">
            <div class="logo">
                <a href="#" class="simple-text">
                    Aplikasi Panjar
                </a>
            </div>
            <div class="logo logo-mini">
                <a href="#" class="simple-text">
                    AP
                </a>
            </div>
            <div class="sidebar-wrapper">
                <div class="user">
                    <div class="photo">
                        <img src="<?= base_url() ?>assets/img/default-avatar.png" />
                    </div>
                    <div class="info">
                        <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                            <?= $this->session->userdata('fullname'); ?>
                            <b class="caret"></b>
                        </a>
                        <div class="collapse" id="collapseExample">
                            <ul class="nav">
                                <li>
                                    <a href="<?= base_url('user/edit/') . $this->session->userdata('id'); ?>">My Profile</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <ul class="nav">

                    <li <?php if ($active == 'Dashboard') { ?> class="active" <?php } ?>>
                        <a href="<?= base_url() ?>admin">
                            <i class="material-icons">dashboard</i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <?php if ($this->session->userdata('role') == 'admin') { ?>
                        <li <?php if ($active == 'User') { ?> class="active" <?php } ?>>
                            <a href="<?= base_url() ?>user">
                                <i class="material-icons">person</i>
                                <p>User Management</p>
                            </a>
                        </li>
                        <li <?php if ($active == 'Unit Kerja') { ?> class="active" <?php } ?>>
                            <a href="<?= base_url() ?>unit">
                                <i class="material-icons">domain</i>
                                <p>Unit Kerja</p>
                            </a>
                        </li>
                    <?php } ?>
                    <li <?php if ($active == 'Pengajuan Panjar' || $active == 'Penyelesaian Panjar') { ?> class="active" <?php } ?>>
                        <a data-toggle="collapse" href="#formsExamples" <?php if ($active == 'Pengajuan Panjar' || $active == 'Penyelesaian Panjar') { ?> aria-expanded="true" <?php } ?>>
                            <i class="material-icons">content_paste</i>
                            <p>Panjar
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse <?php if ($active == 'Pengajuan Panjar' || $active == 'Penyelesaian Panjar') { ?> in <?php } ?>" id="formsExamples">
                            <ul class="nav">
                                <li <?php if ($active == 'Pengajuan Panjar') { ?> class="active" <?php } ?>>
                                    <a href="<?= base_url() ?>transaction/income">Pengajuan Panjar</a>
                                </li>
                                <li <?php if ($active == 'Penyelesaian Panjar') { ?> class="active" <?php } ?>>
                                    <a href="<?= base_url() ?>transaction/expense">Penyelesaian Panjar</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li <?php if ($active == 'RKA') { ?> class="active" <?php } ?>>
                        <a href="<?= base_url() ?>category">
                            <i class="material-icons">view_list</i>
                            <p>Rencana Kerja Anggaran</p>
                        </a>
                    </li>
                    <?php if ($this->session->userdata('role') == 'admin') { ?>
                        <li <?php if ($active == 'Laporan' || $active == 'Laporan-print' || $active == 'Laporan-pengajuan' || $active == 'Laporan-penyelesaian') { ?> class="active" <?php } ?>>
                            <a data-toggle="collapse" href="#formsLaporan" <?php if ($active == 'Laporan') { ?> aria-expanded="true" <?php } ?>>
                                <i class="material-icons">timeline</i>
                                <p>Laporan
                                    <b class="caret"></b>
                                </p>
                            </a>
                            <div class="collapse <?php if ($active == 'Laporan' || $active == 'Laporan-print' || $active == 'Laporan-pengajuan' || $active == 'Laporan-penyelesaian') { ?> in <?php } ?>" id="formsLaporan">
                                <ul class="nav">
                                    <li <?php if ($active == 'Laporan-pengajuan') { ?> class="active" <?php } ?>>
                                        <a href="<?= base_url() ?>transaction/reportIncome">Pengajuan Panjar</a>
                                    </li>
                                    <li <?php if ($active == 'Laporan-penyelesaian') { ?> class="active" <?php } ?>>
                                        <a href="<?= base_url() ?>transaction/reportExpense">Penyelesaian Panjar</a>
                                    </li>
                                    <li <?php if ($active == 'Laporan-print') { ?> class="active" <?php } ?>>
                                        <a href="<?= base_url() ?>transaction/print">Cetak</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-minimize">
                        <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                            <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                            <i class="material-icons visible-on-sidebar-mini">view_list</i>
                        </button>
                    </div>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> <?php echo $headerContent; ?> </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">person</i>
                                    <p class="hidden-lg hidden-md">Profile</p>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="dropdown-toggle">
                                    <p class="hidden-sm hidden-sx"><?= $this->session->userdata('fullname'); ?></p>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>auth/logout">
                                    <i class="material-icons"><span class="material-icons-outlined">
                                            open_in_new
                                        </span></i>
                                    <p class="hidden-lg hidden-md">Logout</p>
                                </a>
                            </li>
                            <li class="separator hidden-lg hidden-md"></li>
                        </ul>
                    </div>
                </div>
            </nav>