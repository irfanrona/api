<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CommentModel extends CI_Model
{

	public $name;
	public $message;
	public $relation;
	public $attendance;

	public function get_last_ten_entries()
	{
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get('comments');
		return $query->result();
	}

	public function insert_entry($initials)
	{
		$this->name    = $_POST['name']; // please read the below note
		$this->message    = $_POST['message'];
		$this->relation    = $_POST['relation'];
		$this->attendance    = $_POST['attendance'];
		$this->initials    = $initials;


		return $this->db->insert('comments', $this);
	}
}
