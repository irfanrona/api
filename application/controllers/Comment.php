<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Comment extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('CommentModel');
		$this->load->library('initials');
	}

	public function index()
	{
		$data['query'] = $this->CommentModel->get_last_ten_entries();
		$json = json_encode($data['query']);

		echo $json;
	}

	public function add()
	{
		//query

		$data = $this->initials->name($_POST['name'])->generate();
		$status = $this->CommentModel->insert_entry($data);

		$previous = "javascript:history.go(-1)";
		if (isset($_SERVER['HTTP_REFERER'])) {
			$previous = $_SERVER['HTTP_REFERER'];
		}
		redirect($previous, 'refresh');
	}

	// This is test function
	public function test()
	{

		// $this->load->library('initials');
		// echo $this->initials->name('Albert Magnum')->generate();
	}
}
